<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {
    return [
        'profile_id' => App\Models\Profile::where('type', 'brand')->inRandomOrder()->first()->id,
        'title' => $faker->name,
        'slug' => str_slug($faker->unique()->name),
        'sizes' => 'xs, xm, xl, l',
        'colors' => 'red, green, yellow, blue',
        'code' => str_random(10),
        'price' => random_int(10, 210),
        'inventory' => random_int(5, 900),
        'can_ship' => ['1', '0'][random_int(0, 1)],
        'description' => $faker->sentences(50, true),
        'distribution_center' => $faker->sentences(20, true), 
        'picture' => 'products/test-product.jpg',
        'is_promoted' => ['1', '0'][random_int(0, 1)],
        'discount' => random_int(0, 100),
    ];
});
