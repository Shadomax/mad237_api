<?php

use Illuminate\Support\Facades\Hash;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Setting::class, function (Faker\Generator $faker) {
    return [
        'app_name' => 'MAD237',
        'currency' => 'francs',
        'phone' => '676788848',
        'email' => 'test@test.com',
        'about' => 'test about',
        'logo' => 'setting/logo.png',
        'favicon' => 'setting/favicon.png',
        'facebook' => 'www.facebook.com',
        'instagram' => 'www.twitter.com',
        'twitter' => 'www.twitter.com',
        'youtube' => 'www.youtube.com',
        'seo_title' => 'Seo Title',
        'seo_keywords' => 'SEO Keywords',
        'seo_description' => 'SEO Description'
    ];
});
