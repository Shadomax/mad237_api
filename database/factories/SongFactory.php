<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Song::class, function (Faker $faker) {
    return [
        'profile_id' => App\Models\Profile::where('type', 'artist')->inRandomOrder()->first()->id,
        'genre_id' => App\Models\Genre::all()->random()->id,
        'producer_id' => App\Models\Profile::where('type', 'producer')->inRandomOrder()->first()->id,
        'album_id' => App\Models\Album::where('type', 'song')->inRandomOrder()->first()->id,
        'title' => $faker->name,
        'picture' => 'songs/new-level.jpg',
        'file' => 'songs/new-level.mp3',
        'preview' => 'songs/preview/new-level.mp3',
        'description' => $faker->sentences(50, true),
        'release_date' => $faker->date,
        'is_promoted' => ['1', '0'][random_int(0, 1)],
    ];
});
