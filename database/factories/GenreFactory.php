<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Genre::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'slug' => str_slug($faker->unique()->name),
        'description' => 'this just text',
    ];
});
