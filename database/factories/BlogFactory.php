<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Blog::class, function (Faker $faker) {
    return [
        'category_id' => App\Models\Category::all()->random()->id,
        'title' => $faker->name,
        'slug' => str_slug($faker->unique()->name),
        'content' => $faker->sentences(50, true),
        'seo_title' => $faker->sentences(1, true),
        'seo_keywords' => $faker->sentences(1, true),
        'seo_description' => $faker->sentences(2, true),
        'picture' => $faker->imageUrl(),
        'is_promoted' => ['1', '0'][random_int(0, 1)],
    ];
});
