<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Photo::class, function (Faker $faker) {

    return [
        'profile_id' => App\Models\Profile::all()->random()->id,
        'picture' => 'profiles/photos/dee-1-im-not-perfect-video.jpg',
    ];
});
