<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->integer('genre_id')->unsigned();
            $table->string('title', 255)->unique();
            $table->string('code', 255)->unique();
            $table->string('price')->default(0);
            $table->string('bpm')->default(0);
            $table->text('description')->nullable();
            $table->string('picture', 255);
            $table->string('preview', 255)->nullable();
            // $table->string('file', 255);
            $table->string('tags', 555)->nullable();
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');

            $table->foreign('genre_id')->references('id')->on('genres')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beats');
    }
}
