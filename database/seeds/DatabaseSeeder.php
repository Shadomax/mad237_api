<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CountriesTableSeeder::class,
            StatesTableSeeder::class,
            CitiesTableSeeder::class,
        ]);
        factory(\App\Models\Setting::class, 1)->create();
        factory(\App\Models\Genre::class, 10)->create();
        factory(\App\Models\Profile::class, 300)->create();
        factory(\App\Models\Album::class, 100)->create();
        factory(\App\Models\Beat::class, 100)->create();
        factory(\App\Models\Song::class, 100)->create();
        factory(\App\Models\Product::class, 50)->create();
        factory(\App\Models\Category::class, 50)->create();
        factory(\App\Models\Blog::class, 500)->create();
        factory(\App\Models\Photo::class, 500)->create();
        factory(\App\Models\Picture::class, 500)->create();
    }
}
