<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('countries')->delete();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Afghanistan',
                'currency' => 'AFN',
                'code' => 'AF',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'Kabul',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'fa-AF,ps,uz-AF,tk',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Albania',
                'currency' => 'ALL',
                'code' => 'AL',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'Tirana',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'sq,el',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Algeria',
                'currency' => 'DZD',
                'code' => 'DZ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Algiers',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'ar-DZ',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'American Samoa',
                'currency' => 'USD',
                'code' => 'AS',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'Pago Pago',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-AS,sm,to',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Andorra',
                'currency' => 'EUR',
                'code' => 'AD',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'Andorra la Vella',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'ca',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Angola',
                'currency' => 'AOA',
                'code' => 'AO',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'Luanda',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'pt-AO',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Anguilla',
                'currency' => 'XCD',
                'code' => 'AI',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'The Valley',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-AI',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Antarctica',
                'currency' => '',
                'code' => 'AQ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => '',
                'continentName' => 'Antarctica',
                'continent' => 'AN',
                'languages' => '',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Antigua And Barbuda',
                'currency' => 'XCD',
                'code' => 'AG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'St. John\'s',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-AG',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Argentina',
                'currency' => 'ARS',
                'code' => 'AR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'Buenos Aires',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'es-AR,en,it,de,fr,gn',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Armenia',
                'currency' => 'AMD',
                'code' => 'AM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'Yerevan',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'hy',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Aruba',
                'currency' => 'AWG',
                'code' => 'AW',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'Oranjestad',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'nl-AW,pap,es,en',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Australia',
                'currency' => 'AUD',
                'code' => 'AU',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'Canberra',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-AU',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Austria',
                'currency' => 'EUR',
                'code' => 'AT',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'Vienna',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'de-AT,hr,hu,sl',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Azerbaijan',
                'currency' => 'AZN',
                'code' => 'AZ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'Baku',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'az,ru,hy',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Bahamas The',
                'currency' => 'BSD',
                'code' => 'BS',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'Nassau',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-BS',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Bahrain',
                'currency' => 'BHD',
                'code' => 'BH',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:54',
                
                'capital' => 'Manama',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ar-BH,en,fa,ur',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Bangladesh',
                'currency' => 'BDT',
                'code' => 'BD',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:54',
                
                'capital' => 'Dhaka',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'bn-BD,en',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Barbados',
                'currency' => 'BBD',
                'code' => 'BB',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:54',
                
                'capital' => 'Bridgetown',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-BB',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Belarus',
                'currency' => 'BYR',
                'code' => 'BY',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'Minsk',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'be,ru',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Belgium',
                'currency' => 'EUR',
                'code' => 'BE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:54',
                
                'capital' => 'Brussels',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'nl-BE,fr-BE,de-BE',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Belize',
                'currency' => 'BZD',
                'code' => 'BZ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'Belmopan',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-BZ,es',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Benin',
                'currency' => 'XOF',
                'code' => 'BJ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:54',
                
                'capital' => 'Porto-Novo',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-BJ',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Bermuda',
                'currency' => 'BMD',
                'code' => 'BM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:54',
                
                'capital' => 'Hamilton',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-BM,pt',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Bhutan',
                'currency' => 'BTN',
                'code' => 'BT',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'Thimphu',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'dz',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Bolivia',
                'currency' => 'BOB',
                'code' => 'BO',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'Sucre',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'es-BO,qu,ay',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Bosnia and Herzegovina',
                'currency' => 'BAM',
                'code' => 'BA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:54',
                
                'capital' => 'Sarajevo',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'bs,hr-BA,sr-BA',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Botswana',
                'currency' => 'BWP',
                'code' => 'BW',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'Gaborone',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-BW,tn-BW',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Bouvet Island',
                'currency' => 'NOK',
                'code' => 'BV',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => '',
                'continentName' => 'Antarctica',
                'continent' => 'AN',
                'languages' => '',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Brazil',
                'currency' => 'BRL',
                'code' => 'BR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'Brasília',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'pt-BR,es,en,fr',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'British Indian Ocean Territory',
                'currency' => 'USD',
                'code' => 'IO',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => '',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'en-IO',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Brunei',
                'currency' => 'BND',
                'code' => 'BN',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'Bandar Seri Begawan',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ms-BN,en-BN',
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Bulgaria',
                'currency' => 'BGN',
                'code' => 'BG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:54',
                
                'capital' => 'Sofia',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'bg,tr-BG,rom',
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Burkina Faso',
                'currency' => 'XOF',
                'code' => 'BF',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:54',
                
                'capital' => 'Ouagadougou',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-BF,mos',
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Burundi',
                'currency' => 'BIF',
                'code' => 'BI',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:54',
                
                'capital' => 'Bujumbura',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-BI,rn',
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Cambodia',
                'currency' => 'KHR',
                'code' => 'KH',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'Phnom Penh',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'km,fr,en',
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'Cameroon',
                'currency' => 'XAF',
                'code' => 'CM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Yaoundé',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-CM,fr-CM',
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Canada',
                'currency' => 'CAD',
                'code' => 'CA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'Ottawa',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-CA,fr-CA,iu',
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Cape Verde',
                'currency' => 'CVE',
                'code' => 'CV',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Praia',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'pt-CV',
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'Cayman Islands',
                'currency' => 'KYD',
                'code' => 'KY',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:00',
                
                'capital' => 'George Town',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-KY',
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Central African Republic',
                'currency' => 'XAF',
                'code' => 'CF',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'Bangui',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-CF,sg,ln,kg',
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'Chad',
                'currency' => 'XAF',
                'code' => 'TD',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:07',
                
                'capital' => 'N\'Djamena',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-TD,ar-TD,sre',
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'Chile',
                'currency' => 'CLP',
                'code' => 'CL',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Santiago',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'es-CL',
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'China',
                'currency' => 'CNY',
                'code' => 'CN',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Beijing',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'zh-CN,yue,wuu,dta,ug,za',
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'Christmas Island',
                'currency' => 'AUD',
                'code' => 'CX',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Flying Fish Cove',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'en,zh,ms-CC',
            ),
            45 => 
            array (
                'id' => 46,
            'name' => 'Cocos (Keeling) Islands',
                'currency' => 'AUD',
                'code' => 'CC',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'West Island',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ms-CC,en',
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'Colombia',
                'currency' => 'COP',
                'code' => 'CO',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Bogotá',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'es-CO',
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'Comoros',
                'currency' => 'KMF',
                'code' => 'KM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:00',
                
                'capital' => 'Moroni',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'ar,fr-KM',
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Congo',
                'currency' => 'XAF',
                'code' => 'CG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'Brazzaville',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-CG,kg,ln-CG',
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Congo The Democratic Republic Of The',
                'currency' => 'CDF',
                'code' => 'CD',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:55',
                
                'capital' => 'Kinshasa',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-CD,ln,ktu,kg,sw,lua',
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Cook Islands',
                'currency' => 'NZD',
                'code' => 'CK',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Avarua',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-CK,mi',
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Costa Rica',
                'currency' => 'CRC',
                'code' => 'CR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'San José',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'es-CR,en',
            ),
            52 => 
            array (
                'id' => 53,
            'name' => 'Cote D\'Ivoire (Ivory Coast)',
                'currency' => 'XOF',
                'code' => 'CI',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Yamoussoukro',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-CI',
            ),
            53 => 
            array (
                'id' => 54,
            'name' => 'Croatia (Hrvatska)',
                'currency' => 'HRK',
                'code' => 'HR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Zagreb',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'hr-HR,sr',
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'Cuba',
                'currency' => 'CUP',
                'code' => 'CU',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Havana',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'es-CU,pap',
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Cyprus',
                'currency' => 'EUR',
                'code' => 'CY',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Nicosia',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'el-CY,tr-CY,en',
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'Czech Republic',
                'currency' => 'CZK',
                'code' => 'CZ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Prague',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'cs,sk',
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Denmark',
                'currency' => 'DKK',
                'code' => 'DK',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Copenhagen',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'da-DK,en,fo,de-DK',
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'Djibouti',
                'currency' => 'DJF',
                'code' => 'DJ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Djibouti',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-DJ,ar,so-DJ,aa',
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'Dominica',
                'currency' => 'XCD',
                'code' => 'DM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Roseau',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-DM',
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Dominican Republic',
                'currency' => 'DOP',
                'code' => 'DO',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Santo Domingo',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'es-DO',
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'East Timor',
                'currency' => '',
                'code' => 'TP',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2016-11-21 05:22:22',
                
                'capital' => '',
                'continentName' => '',
                'continent' => '',
                'languages' => '',
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'Ecuador',
                'currency' => 'USD',
                'code' => 'EC',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Quito',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'es-EC',
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'Egypt',
                'currency' => 'EGP',
                'code' => 'EG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Cairo',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'ar-EG,en,fr',
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'El Salvador',
                'currency' => 'USD',
                'code' => 'SV',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:07',
                
                'capital' => 'San Salvador',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'es-SV',
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'Equatorial Guinea',
                'currency' => 'XAF',
                'code' => 'GQ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Malabo',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'es-GQ,fr',
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'Eritrea',
                'currency' => 'ERN',
                'code' => 'ER',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Asmara',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'aa-ER,ar,tig,kun,ti-ER',
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'Estonia',
                'currency' => 'EUR',
                'code' => 'EE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Tallinn',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'et,ru',
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Ethiopia',
                'currency' => 'ETB',
                'code' => 'ET',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Addis Ababa',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'am,en-ET,om-ET,ti-ET,so-ET,sid',
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'External Territories of Australia',
                'currency' => '',
                'code' => 'XA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2016-11-21 05:22:22',
                
                'capital' => '',
                'continentName' => '',
                'continent' => '',
                'languages' => '',
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Falkland Islands',
                'currency' => 'FKP',
                'code' => 'FK',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Stanley',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'en-FK',
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'Faroe Islands',
                'currency' => 'DKK',
                'code' => 'FO',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Tórshavn',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'fo,da-FO',
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Fiji Islands',
                'currency' => 'FJD',
                'code' => 'FJ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Suva',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-FJ,fj',
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Finland',
                'currency' => 'EUR',
                'code' => 'FI',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Helsinki',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'fi-FI,sv-FI,smn',
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'France',
                'currency' => 'EUR',
                'code' => 'FR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Paris',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'fr-FR,frp,br,co,ca,eu,oc',
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'French Guiana',
                'currency' => 'EUR',
                'code' => 'GF',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Cayenne',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'fr-GF',
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'French Polynesia',
                'currency' => 'XPF',
                'code' => 'PF',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:04',
                
                'capital' => 'Papeete',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'fr-PF,ty',
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'French Southern Territories',
                'currency' => 'EUR',
                'code' => 'TF',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:07',
                
                'capital' => 'Port-aux-Français',
                'continentName' => 'Antarctica',
                'continent' => 'AN',
                'languages' => 'fr',
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'Gabon',
                'currency' => 'XAF',
                'code' => 'GA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Libreville',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-GA',
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Gambia The',
                'currency' => 'GMD',
                'code' => 'GM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Bathurst',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-GM,mnk,wof,wo,ff',
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Georgia',
                'currency' => 'GEL',
                'code' => 'GE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Tbilisi',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ka,ru,hy,az',
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Germany',
                'currency' => 'EUR',
                'code' => 'DE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Berlin',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'de',
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'Ghana',
                'currency' => 'GHS',
                'code' => 'GH',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Accra',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-GH,ak,ee,tw',
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Gibraltar',
                'currency' => 'GIP',
                'code' => 'GI',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Gibraltar',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'en-GI,es,it,pt',
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'Greece',
                'currency' => 'EUR',
                'code' => 'GR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Athens',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'el-GR,en,fr',
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'Greenland',
                'currency' => 'DKK',
                'code' => 'GL',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Nuuk',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'kl,da-GL,en',
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'Grenada',
                'currency' => 'XCD',
                'code' => 'GD',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'St. George\'s',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-GD',
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'Guadeloupe',
                'currency' => 'EUR',
                'code' => 'GP',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Basse-Terre',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'fr-GP',
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'Guam',
                'currency' => 'USD',
                'code' => 'GU',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Hagåtña',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-GU,ch-GU',
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'Guatemala',
                'currency' => 'GTQ',
                'code' => 'GT',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Guatemala City',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'es-GT',
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'Guernsey and Alderney',
                'currency' => '',
                'code' => 'XU',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2016-11-21 05:22:22',
                
                'capital' => '',
                'continentName' => '',
                'continent' => '',
                'languages' => '',
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'Guinea',
                'currency' => 'GNF',
                'code' => 'GN',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Conakry',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-GN',
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'Guinea-Bissau',
                'currency' => 'XOF',
                'code' => 'GW',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Bissau',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'pt-GW,pov',
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'Guyana',
                'currency' => 'GYD',
                'code' => 'GY',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Georgetown',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'en-GY',
            ),
            94 => 
            array (
                'id' => 95,
                'name' => 'Haiti',
                'currency' => 'HTG',
                'code' => 'HT',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Port-au-Prince',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'ht,fr-HT',
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'Heard and McDonald Islands',
                'currency' => 'AUD',
                'code' => 'HM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => '',
                'continentName' => 'Antarctica',
                'continent' => 'AN',
                'languages' => '',
            ),
            96 => 
            array (
                'id' => 97,
                'name' => 'Honduras',
                'currency' => 'HNL',
                'code' => 'HN',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Tegucigalpa',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'es-HN,cab,miq',
            ),
            97 => 
            array (
                'id' => 98,
                'name' => 'Hong Kong S.A.R.',
                'currency' => 'HKD',
                'code' => 'HK',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Hong Kong',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'zh-HK,yue,zh,en',
            ),
            98 => 
            array (
                'id' => 99,
                'name' => 'Hungary',
                'currency' => 'HUF',
                'code' => 'HU',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Budapest',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'hu-HU',
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'Iceland',
                'currency' => 'ISK',
                'code' => 'IS',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'Reykjavik',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'is,en,de,da,sv,no',
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'India',
                'currency' => 'INR',
                'code' => 'IN',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'New Delhi',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'en-IN,hi,bn,te,mr,ta,ur,gu,kn,ml,or,pa,as,bh,sat,ks,ne,sd,kok,doi,mni,sit,sa,fr,lus,inc',
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'Indonesia',
                'currency' => 'IDR',
                'code' => 'ID',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Jakarta',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'id,en,nl,jv',
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'Iran',
                'currency' => 'IRR',
                'code' => 'IR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'Tehran',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'fa-IR,ku',
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'Iraq',
                'currency' => 'IQD',
                'code' => 'IQ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'Baghdad',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ar-IQ,ku,hy',
            ),
            104 => 
            array (
                'id' => 105,
                'name' => 'Ireland',
                'currency' => 'EUR',
                'code' => 'IE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'Dublin',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'en-IE,ga-IE',
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'Israel',
                'currency' => 'ILS',
                'code' => 'IL',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => '',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'he,ar-IL,en-IL,',
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'Italy',
                'currency' => 'EUR',
                'code' => 'IT',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'Rome',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'it-IT,de-IT,fr-IT,sc,ca,co,sl',
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'Jamaica',
                'currency' => 'JMD',
                'code' => 'JM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'Kingston',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-JM',
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'Japan',
                'currency' => 'JPY',
                'code' => 'JP',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'Tokyo',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ja',
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'Jersey',
                'currency' => '',
                'code' => 'XJ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2016-11-21 05:22:22',
                
                'capital' => '',
                'continentName' => '',
                'continent' => '',
                'languages' => '',
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'Jordan',
                'currency' => 'JOD',
                'code' => 'JO',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'Amman',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ar-JO,en',
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'Kazakhstan',
                'currency' => 'KZT',
                'code' => 'KZ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:00',
                
                'capital' => 'Astana',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'kk,ru',
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'Kenya',
                'currency' => 'KES',
                'code' => 'KE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'Nairobi',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-KE,sw-KE',
            ),
            113 => 
            array (
                'id' => 114,
                'name' => 'Kiribati',
                'currency' => 'AUD',
                'code' => 'KI',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'Tarawa',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-KI,gil',
            ),
            114 => 
            array (
                'id' => 115,
                'name' => 'Korea North',
                'currency' => 'KPW',
                'code' => 'KP',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:00',
                
                'capital' => 'Pyongyang',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ko-KP',
            ),
            115 => 
            array (
                'id' => 116,
                'name' => 'Korea South',
                'currency' => 'KRW',
                'code' => 'KR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:00',
                
                'capital' => 'Seoul',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ko-KR,en',
            ),
            116 => 
            array (
                'id' => 117,
                'name' => 'Kuwait',
                'currency' => 'KWD',
                'code' => 'KW',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:00',
                
                'capital' => 'Kuwait City',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ar-KW,en',
            ),
            117 => 
            array (
                'id' => 118,
                'name' => 'Kyrgyzstan',
                'currency' => 'KGS',
                'code' => 'KG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:59',
                
                'capital' => 'Bishkek',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ky,uz,ru',
            ),
            118 => 
            array (
                'id' => 119,
                'name' => 'Laos',
                'currency' => 'LAK',
                'code' => 'LA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:00',
                
                'capital' => 'Vientiane',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'lo,fr,en',
            ),
            119 => 
            array (
                'id' => 120,
                'name' => 'Latvia',
                'currency' => 'EUR',
                'code' => 'LV',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:01',
                
                'capital' => 'Riga',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'lv,ru,lt',
            ),
            120 => 
            array (
                'id' => 121,
                'name' => 'Lebanon',
                'currency' => 'LBP',
                'code' => 'LB',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:00',
                
                'capital' => 'Beirut',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ar-LB,fr-LB,en,hy',
            ),
            121 => 
            array (
                'id' => 122,
                'name' => 'Lesotho',
                'currency' => 'LSL',
                'code' => 'LS',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:01',
                
                'capital' => 'Maseru',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-LS,st,zu,xh',
            ),
            122 => 
            array (
                'id' => 123,
                'name' => 'Liberia',
                'currency' => 'LRD',
                'code' => 'LR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:01',
                
                'capital' => 'Monrovia',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-LR',
            ),
            123 => 
            array (
                'id' => 124,
                'name' => 'Libya',
                'currency' => 'LYD',
                'code' => 'LY',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:01',
                
                'capital' => 'Tripoli',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'ar-LY,it,en',
            ),
            124 => 
            array (
                'id' => 125,
                'name' => 'Liechtenstein',
                'currency' => 'CHF',
                'code' => 'LI',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:00',
                
                'capital' => 'Vaduz',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'de-LI',
            ),
            125 => 
            array (
                'id' => 126,
                'name' => 'Lithuania',
                'currency' => 'EUR',
                'code' => 'LT',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:01',
                
                'capital' => 'Vilnius',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'lt,ru,pl',
            ),
            126 => 
            array (
                'id' => 127,
                'name' => 'Luxembourg',
                'currency' => 'EUR',
                'code' => 'LU',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:01',
                
                'capital' => 'Luxembourg',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'lb,de-LU,fr-LU',
            ),
            127 => 
            array (
                'id' => 128,
                'name' => 'Macau S.A.R.',
                'currency' => 'MOP',
                'code' => 'MO',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Macao',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'zh,zh-MO,pt',
            ),
            128 => 
            array (
                'id' => 129,
                'name' => 'Macedonia',
                'currency' => 'MKD',
                'code' => 'MK',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Skopje',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'mk,sq,tr,rmm,sr',
            ),
            129 => 
            array (
                'id' => 130,
                'name' => 'Madagascar',
                'currency' => 'MGA',
                'code' => 'MG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:01',
                
                'capital' => 'Antananarivo',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-MG,mg',
            ),
            130 => 
            array (
                'id' => 131,
                'name' => 'Malawi',
                'currency' => 'MWK',
                'code' => 'MW',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Lilongwe',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'ny,yao,tum,swk',
            ),
            131 => 
            array (
                'id' => 132,
                'name' => 'Malaysia',
                'currency' => 'MYR',
                'code' => 'MY',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Kuala Lumpur',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ms-MY,en,zh,ta,te,ml,pa,th',
            ),
            132 => 
            array (
                'id' => 133,
                'name' => 'Maldives',
                'currency' => 'MVR',
                'code' => 'MV',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Malé',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'dv,en',
            ),
            133 => 
            array (
                'id' => 134,
                'name' => 'Mali',
                'currency' => 'XOF',
                'code' => 'ML',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Bamako',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-ML,bm',
            ),
            134 => 
            array (
                'id' => 135,
                'name' => 'Malta',
                'currency' => 'EUR',
                'code' => 'MT',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Valletta',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'mt,en-MT',
            ),
            135 => 
            array (
                'id' => 136,
            'name' => 'Man (Isle of)',
                'currency' => '',
                'code' => 'XM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2016-11-21 05:22:22',
                
                'capital' => '',
                'continentName' => '',
                'continent' => '',
                'languages' => '',
            ),
            136 => 
            array (
                'id' => 137,
                'name' => 'Marshall Islands',
                'currency' => 'USD',
                'code' => 'MH',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:01',
                
                'capital' => 'Majuro',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'mh,en-MH',
            ),
            137 => 
            array (
                'id' => 138,
                'name' => 'Martinique',
                'currency' => 'EUR',
                'code' => 'MQ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Fort-de-France',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'fr-MQ',
            ),
            138 => 
            array (
                'id' => 139,
                'name' => 'Mauritania',
                'currency' => 'MRO',
                'code' => 'MR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Nouakchott',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'ar-MR,fuc,snk,fr,mey,wo',
            ),
            139 => 
            array (
                'id' => 140,
                'name' => 'Mauritius',
                'currency' => 'MUR',
                'code' => 'MU',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Port Louis',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-MU,bho,fr',
            ),
            140 => 
            array (
                'id' => 141,
                'name' => 'Mayotte',
                'currency' => 'EUR',
                'code' => 'YT',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Mamoudzou',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-YT',
            ),
            141 => 
            array (
                'id' => 142,
                'name' => 'Mexico',
                'currency' => 'MXN',
                'code' => 'MX',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Mexico City',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'es-MX',
            ),
            142 => 
            array (
                'id' => 143,
                'name' => 'Micronesia',
                'currency' => 'USD',
                'code' => 'FM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Palikir',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-FM,chk,pon,yap,kos,uli,woe,nkr,kpg',
            ),
            143 => 
            array (
                'id' => 144,
                'name' => 'Moldova',
                'currency' => 'MDL',
                'code' => 'MD',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:01',
                
                'capital' => 'Chişinău',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'ro,ru,gag,tr',
            ),
            144 => 
            array (
                'id' => 145,
                'name' => 'Monaco',
                'currency' => 'EUR',
                'code' => 'MC',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:01',
                
                'capital' => 'Monaco',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'fr-MC,en,it',
            ),
            145 => 
            array (
                'id' => 146,
                'name' => 'Mongolia',
                'currency' => 'MNT',
                'code' => 'MN',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Ulan Bator',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'mn,ru',
            ),
            146 => 
            array (
                'id' => 147,
                'name' => 'Montserrat',
                'currency' => 'XCD',
                'code' => 'MS',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Plymouth',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-MS',
            ),
            147 => 
            array (
                'id' => 148,
                'name' => 'Morocco',
                'currency' => 'MAD',
                'code' => 'MA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:01',
                
                'capital' => 'Rabat',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'ar-MA,ber,fr',
            ),
            148 => 
            array (
                'id' => 149,
                'name' => 'Mozambique',
                'currency' => 'MZN',
                'code' => 'MZ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Maputo',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'pt-MZ,vmw',
            ),
            149 => 
            array (
                'id' => 150,
                'name' => 'Myanmar',
                'currency' => 'MMK',
                'code' => 'MM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Naypyitaw',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'my',
            ),
            150 => 
            array (
                'id' => 151,
                'name' => 'Namibia',
                'currency' => 'NAD',
                'code' => 'NA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Windhoek',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-NA,af,de,hz,naq',
            ),
            151 => 
            array (
                'id' => 152,
                'name' => 'Nauru',
                'currency' => 'AUD',
                'code' => 'NR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:04',
                
                'capital' => 'Yaren',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'na,en-NR',
            ),
            152 => 
            array (
                'id' => 153,
                'name' => 'Nepal',
                'currency' => 'NPR',
                'code' => 'NP',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:03',
                
                'capital' => 'Kathmandu',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ne,en',
            ),
            153 => 
            array (
                'id' => 154,
                'name' => 'Netherlands Antilles',
                'currency' => '',
                'code' => 'AN',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2016-11-21 05:22:22',
                
                'capital' => '',
                'continentName' => '',
                'continent' => '',
                'languages' => '',
            ),
            154 => 
            array (
                'id' => 155,
                'name' => 'Netherlands The',
                'currency' => 'EUR',
                'code' => 'NL',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:03',
                
                'capital' => 'Amsterdam',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'nl-NL,fy-NL',
            ),
            155 => 
            array (
                'id' => 156,
                'name' => 'New Caledonia',
                'currency' => 'XPF',
                'code' => 'NC',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Noumea',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'fr-NC',
            ),
            156 => 
            array (
                'id' => 157,
                'name' => 'New Zealand',
                'currency' => 'NZD',
                'code' => 'NZ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:04',
                
                'capital' => 'Wellington',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-NZ,mi',
            ),
            157 => 
            array (
                'id' => 158,
                'name' => 'Nicaragua',
                'currency' => 'NIO',
                'code' => 'NI',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:03',
                
                'capital' => 'Managua',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'es-NI,en',
            ),
            158 => 
            array (
                'id' => 159,
                'name' => 'Niger',
                'currency' => 'XOF',
                'code' => 'NE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:03',
                
                'capital' => 'Niamey',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-NE,ha,kr,dje',
            ),
            159 => 
            array (
                'id' => 160,
                'name' => 'Nigeria',
                'currency' => 'NGN',
                'code' => 'NG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:03',
                
                'capital' => 'Abuja',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-NG,ha,yo,ig,ff',
            ),
            160 => 
            array (
                'id' => 161,
                'name' => 'Niue',
                'currency' => 'NZD',
                'code' => 'NU',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:04',
                
                'capital' => 'Alofi',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'niu,en-NU',
            ),
            161 => 
            array (
                'id' => 162,
                'name' => 'Norfolk Island',
                'currency' => 'AUD',
                'code' => 'NF',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:03',
                
                'capital' => 'Kingston',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-NF',
            ),
            162 => 
            array (
                'id' => 163,
                'name' => 'Northern Mariana Islands',
                'currency' => 'USD',
                'code' => 'MP',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:02',
                
                'capital' => 'Saipan',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'fil,tl,zh,ch-MP,en-MP',
            ),
            163 => 
            array (
                'id' => 164,
                'name' => 'Norway',
                'currency' => 'NOK',
                'code' => 'NO',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:03',
                
                'capital' => 'Oslo',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'no,nb,nn,se,fi',
            ),
            164 => 
            array (
                'id' => 165,
                'name' => 'Oman',
                'currency' => 'OMR',
                'code' => 'OM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:04',
                
                'capital' => 'Muscat',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ar-OM,en,bal,ur',
            ),
            165 => 
            array (
                'id' => 166,
                'name' => 'Pakistan',
                'currency' => 'PKR',
                'code' => 'PK',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:04',
                
                'capital' => 'Islamabad',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ur-PK,en-PK,pa,sd,ps,brh',
            ),
            166 => 
            array (
                'id' => 167,
                'name' => 'Palau',
                'currency' => 'USD',
                'code' => 'PW',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'Melekeok',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'pau,sov,en-PW,tox,ja,fil,zh',
            ),
            167 => 
            array (
                'id' => 168,
                'name' => 'Palestinian Territory Occupied',
                'currency' => 'ILS',
                'code' => 'PS',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => '',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ar-PS',
            ),
            168 => 
            array (
                'id' => 169,
                'name' => 'Panama',
                'currency' => 'PAB',
                'code' => 'PA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:04',
                
                'capital' => 'Panama City',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'es-PA,en',
            ),
            169 => 
            array (
                'id' => 170,
                'name' => 'Papua new Guinea',
                'currency' => 'PGK',
                'code' => 'PG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:04',
                
                'capital' => 'Port Moresby',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-PG,ho,meu,tpi',
            ),
            170 => 
            array (
                'id' => 171,
                'name' => 'Paraguay',
                'currency' => 'PYG',
                'code' => 'PY',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'Asunción',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'es-PY,gn',
            ),
            171 => 
            array (
                'id' => 172,
                'name' => 'Peru',
                'currency' => 'PEN',
                'code' => 'PE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:04',
                
                'capital' => 'Lima',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'es-PE,qu,ay',
            ),
            172 => 
            array (
                'id' => 173,
                'name' => 'Philippines',
                'currency' => 'PHP',
                'code' => 'PH',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:04',
                
                'capital' => 'Manila',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'tl,en-PH,fil,ceb,tgl,ilo,hil,war,pam,bik,bcl,pag,mrw,tsg,mdh,cbk,krj,sgd,msb,akl,ibg,yka,mta,abx',
            ),
            173 => 
            array (
                'id' => 174,
                'name' => 'Pitcairn Island',
                'currency' => 'NZD',
                'code' => 'PN',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'Adamstown',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-PN',
            ),
            174 => 
            array (
                'id' => 175,
                'name' => 'Poland',
                'currency' => 'PLN',
                'code' => 'PL',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:04',
                
                'capital' => 'Warsaw',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'pl',
            ),
            175 => 
            array (
                'id' => 176,
                'name' => 'Portugal',
                'currency' => 'EUR',
                'code' => 'PT',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'Lisbon',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'pt-PT,mwl',
            ),
            176 => 
            array (
                'id' => 177,
                'name' => 'Puerto Rico',
                'currency' => 'USD',
                'code' => 'PR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'San Juan',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-PR,es-PR',
            ),
            177 => 
            array (
                'id' => 178,
                'name' => 'Qatar',
                'currency' => 'QAR',
                'code' => 'QA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'Doha',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ar-QA,es',
            ),
            178 => 
            array (
                'id' => 179,
                'name' => 'Reunion',
                'currency' => 'EUR',
                'code' => 'RE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'Saint-Denis',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-RE',
            ),
            179 => 
            array (
                'id' => 180,
                'name' => 'Romania',
                'currency' => 'RON',
                'code' => 'RO',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'Bucharest',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'ro,hu,rom',
            ),
            180 => 
            array (
                'id' => 181,
                'name' => 'Russia',
                'currency' => 'RUB',
                'code' => 'RU',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'Moscow',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'ru,tt,xal,cau,ady,kv,ce,tyv,cv,udm,tut,mns,bua,myv,mdf,chm,ba,inh,tut,kbd,krc,ava,sah,nog',
            ),
            181 => 
            array (
                'id' => 182,
                'name' => 'Rwanda',
                'currency' => 'RWF',
                'code' => 'RW',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'Kigali',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'rw,en-RW,fr-RW,sw',
            ),
            182 => 
            array (
                'id' => 183,
                'name' => 'Saint Helena',
                'currency' => 'SHP',
                'code' => 'SH',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Jamestown',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-SH',
            ),
            183 => 
            array (
                'id' => 184,
                'name' => 'Saint Kitts And Nevis',
                'currency' => 'XCD',
                'code' => 'KN',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:00',
                
                'capital' => 'Basseterre',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-KN',
            ),
            184 => 
            array (
                'id' => 185,
                'name' => 'Saint Lucia',
                'currency' => 'XCD',
                'code' => 'LC',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:00',
                
                'capital' => 'Castries',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-LC',
            ),
            185 => 
            array (
                'id' => 186,
                'name' => 'Saint Pierre and Miquelon',
                'currency' => 'EUR',
                'code' => 'PM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'Saint-Pierre',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'fr-PM',
            ),
            186 => 
            array (
                'id' => 187,
                'name' => 'Saint Vincent And The Grenadines',
                'currency' => 'XCD',
                'code' => 'VC',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Kingstown',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-VC,fr',
            ),
            187 => 
            array (
                'id' => 188,
                'name' => 'Samoa',
                'currency' => 'WST',
                'code' => 'WS',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Apia',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'sm,en-WS',
            ),
            188 => 
            array (
                'id' => 189,
                'name' => 'San Marino',
                'currency' => 'EUR',
                'code' => 'SM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'San Marino',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'it-SM',
            ),
            189 => 
            array (
                'id' => 190,
                'name' => 'Sao Tome and Principe',
                'currency' => 'STD',
                'code' => 'ST',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'São Tomé',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'pt-ST',
            ),
            190 => 
            array (
                'id' => 191,
                'name' => 'Saudi Arabia',
                'currency' => 'SAR',
                'code' => 'SA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'Riyadh',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ar-SA',
            ),
            191 => 
            array (
                'id' => 192,
                'name' => 'Senegal',
                'currency' => 'XOF',
                'code' => 'SN',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Dakar',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-SN,wo,fuc,mnk',
            ),
            192 => 
            array (
                'id' => 193,
                'name' => 'Serbia',
                'currency' => 'RSD',
                'code' => 'RS',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:05',
                
                'capital' => 'Belgrade',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'sr,hu,bs,rom',
            ),
            193 => 
            array (
                'id' => 194,
                'name' => 'Seychelles',
                'currency' => 'SCR',
                'code' => 'SC',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Victoria',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-SC,fr-SC',
            ),
            194 => 
            array (
                'id' => 195,
                'name' => 'Sierra Leone',
                'currency' => 'SLL',
                'code' => 'SL',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Freetown',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-SL,men,tem',
            ),
            195 => 
            array (
                'id' => 196,
                'name' => 'Singapore',
                'currency' => 'SGD',
                'code' => 'SG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Singapore',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'cmn,en-SG,ms-SG,ta-SG,zh-SG',
            ),
            196 => 
            array (
                'id' => 197,
                'name' => 'Slovakia',
                'currency' => 'EUR',
                'code' => 'SK',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Bratislava',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'sk,hu',
            ),
            197 => 
            array (
                'id' => 198,
                'name' => 'Slovenia',
                'currency' => 'EUR',
                'code' => 'SI',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Ljubljana',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'sl,sh',
            ),
            198 => 
            array (
                'id' => 199,
                'name' => 'Smaller Territories of the UK',
                'currency' => '',
                'code' => 'XG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2016-11-21 05:22:22',
                
                'capital' => '',
                'continentName' => '',
                'continent' => '',
                'languages' => '',
            ),
            199 => 
            array (
                'id' => 200,
                'name' => 'Solomon Islands',
                'currency' => 'SBD',
                'code' => 'SB',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Honiara',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-SB,tpi',
            ),
            200 => 
            array (
                'id' => 201,
                'name' => 'Somalia',
                'currency' => 'SOS',
                'code' => 'SO',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Mogadishu',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'so-SO,ar-SO,it,en-SO',
            ),
            201 => 
            array (
                'id' => 202,
                'name' => 'South Africa',
                'currency' => 'ZAR',
                'code' => 'ZA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Pretoria',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'zu,xh,af,nso,en-ZA,tn,st,ts,ss,ve,nr',
            ),
            202 => 
            array (
                'id' => 203,
                'name' => 'South Georgia',
                'currency' => 'GBP',
                'code' => 'GS',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:58',
                
                'capital' => 'Grytviken',
                'continentName' => 'Antarctica',
                'continent' => 'AN',
                'languages' => 'en',
            ),
            203 => 
            array (
                'id' => 204,
                'name' => 'South Sudan',
                'currency' => 'SSP',
                'code' => 'SS',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Juba',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en',
            ),
            204 => 
            array (
                'id' => 205,
                'name' => 'Spain',
                'currency' => 'EUR',
                'code' => 'ES',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Madrid',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'es-ES,ca,gl,eu,oc',
            ),
            205 => 
            array (
                'id' => 206,
                'name' => 'Sri Lanka',
                'currency' => 'LKR',
                'code' => 'LK',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:01',
                
                'capital' => 'Colombo',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'si,ta,en',
            ),
            206 => 
            array (
                'id' => 207,
                'name' => 'Sudan',
                'currency' => 'SDG',
                'code' => 'SD',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Khartoum',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'ar-SD,en,fia',
            ),
            207 => 
            array (
                'id' => 208,
                'name' => 'Suriname',
                'currency' => 'SRD',
                'code' => 'SR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Paramaribo',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'nl-SR,en,srn,hns,jv',
            ),
            208 => 
            array (
                'id' => 209,
                'name' => 'Svalbard And Jan Mayen Islands',
                'currency' => 'NOK',
                'code' => 'SJ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Longyearbyen',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'no,ru',
            ),
            209 => 
            array (
                'id' => 210,
                'name' => 'Swaziland',
                'currency' => 'SZL',
                'code' => 'SZ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:07',
                
                'capital' => 'Mbabane',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-SZ,ss-SZ',
            ),
            210 => 
            array (
                'id' => 211,
                'name' => 'Sweden',
                'currency' => 'SEK',
                'code' => 'SE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:06',
                
                'capital' => 'Stockholm',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'sv-SE,se,sma,fi-SE',
            ),
            211 => 
            array (
                'id' => 212,
                'name' => 'Switzerland',
                'currency' => 'CHF',
                'code' => 'CH',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:56',
                
                'capital' => 'Bern',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'de-CH,fr-CH,it-CH,rm',
            ),
            212 => 
            array (
                'id' => 213,
                'name' => 'Syria',
                'currency' => 'SYP',
                'code' => 'SY',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:07',
                
                'capital' => 'Damascus',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ar-SY,ku,hy,arc,fr,en',
            ),
            213 => 
            array (
                'id' => 214,
                'name' => 'Taiwan',
                'currency' => 'TWD',
                'code' => 'TW',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:08',
                
                'capital' => 'Taipei',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'zh-TW,zh,nan,hak',
            ),
            214 => 
            array (
                'id' => 215,
                'name' => 'Tajikistan',
                'currency' => 'TJS',
                'code' => 'TJ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:07',
                
                'capital' => 'Dushanbe',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'tg,ru',
            ),
            215 => 
            array (
                'id' => 216,
                'name' => 'Tanzania',
                'currency' => 'TZS',
                'code' => 'TZ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:08',
                
                'capital' => 'Dodoma',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'sw-TZ,en,ar',
            ),
            216 => 
            array (
                'id' => 217,
                'name' => 'Thailand',
                'currency' => 'THB',
                'code' => 'TH',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:07',
                
                'capital' => 'Bangkok',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'th,en',
            ),
            217 => 
            array (
                'id' => 218,
                'name' => 'Togo',
                'currency' => 'XOF',
                'code' => 'TG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:07',
                
                'capital' => 'Lomé',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'fr-TG,ee,hna,kbp,dag,ha',
            ),
            218 => 
            array (
                'id' => 219,
                'name' => 'Tokelau',
                'currency' => 'NZD',
                'code' => 'TK',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:07',
                
                'capital' => '',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'tkl,en-TK',
            ),
            219 => 
            array (
                'id' => 220,
                'name' => 'Tonga',
                'currency' => 'TOP',
                'code' => 'TO',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:08',
                
                'capital' => 'Nuku\'alofa',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'to,en-TO',
            ),
            220 => 
            array (
                'id' => 221,
                'name' => 'Trinidad And Tobago',
                'currency' => 'TTD',
                'code' => 'TT',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:08',
                
                'capital' => 'Port of Spain',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-TT,hns,fr,es,zh',
            ),
            221 => 
            array (
                'id' => 222,
                'name' => 'Tunisia',
                'currency' => 'TND',
                'code' => 'TN',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:08',
                
                'capital' => 'Tunis',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'ar-TN,fr',
            ),
            222 => 
            array (
                'id' => 223,
                'name' => 'Turkey',
                'currency' => 'TRY',
                'code' => 'TR',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:08',
                
                'capital' => 'Ankara',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'tr-TR,ku,diq,az,av',
            ),
            223 => 
            array (
                'id' => 224,
                'name' => 'Turkmenistan',
                'currency' => 'TMT',
                'code' => 'TM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:07',
                
                'capital' => 'Ashgabat',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'tk,ru,uz',
            ),
            224 => 
            array (
                'id' => 225,
                'name' => 'Turks And Caicos Islands',
                'currency' => 'USD',
                'code' => 'TC',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:07',
                
                'capital' => 'Cockburn Town',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-TC',
            ),
            225 => 
            array (
                'id' => 226,
                'name' => 'Tuvalu',
                'currency' => 'AUD',
                'code' => 'TV',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:08',
                
                'capital' => 'Funafuti',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'tvl,en,sm,gil',
            ),
            226 => 
            array (
                'id' => 227,
                'name' => 'Uganda',
                'currency' => 'UGX',
                'code' => 'UG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Kampala',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-UG,lg,sw,ar',
            ),
            227 => 
            array (
                'id' => 228,
                'name' => 'Ukraine',
                'currency' => 'UAH',
                'code' => 'UA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:08',
                
                'capital' => 'Kiev',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'uk,ru-UA,rom,pl,hu',
            ),
            228 => 
            array (
                'id' => 229,
                'name' => 'United Arab Emirates',
                'currency' => 'AED',
                'code' => 'AE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:53',
                
                'capital' => 'Abu Dhabi',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ar-AE,fa,en,hi,ur',
            ),
            229 => 
            array (
                'id' => 230,
                'name' => 'United Kingdom',
                'currency' => 'GBP',
                'code' => 'GB',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'London',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'en-GB,cy-GB,gd',
            ),
            230 => 
            array (
                'id' => 231,
                'name' => 'United States',
                'currency' => 'USD',
                'code' => 'US',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Washington',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-US,es-US,haw,fr',
            ),
            231 => 
            array (
                'id' => 232,
                'name' => 'United States Minor Outlying Islands',
                'currency' => 'USD',
                'code' => 'UM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => '',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'en-UM',
            ),
            232 => 
            array (
                'id' => 233,
                'name' => 'Uruguay',
                'currency' => 'UYU',
                'code' => 'UY',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Montevideo',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'es-UY',
            ),
            233 => 
            array (
                'id' => 234,
                'name' => 'Uzbekistan',
                'currency' => 'UZS',
                'code' => 'UZ',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Tashkent',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'uz,ru,tg',
            ),
            234 => 
            array (
                'id' => 235,
                'name' => 'Vanuatu',
                'currency' => 'VUV',
                'code' => 'VU',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Port Vila',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'bi,en-VU,fr-VU',
            ),
            235 => 
            array (
                'id' => 236,
            'name' => 'Vatican City State (Holy See)',
                'currency' => 'EUR',
                'code' => 'VA',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Vatican City',
                'continentName' => 'Europe',
                'continent' => 'EU',
                'languages' => 'la,it,fr',
            ),
            236 => 
            array (
                'id' => 237,
                'name' => 'Venezuela',
                'currency' => 'VEF',
                'code' => 'VE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Caracas',
                'continentName' => 'South America',
                'continent' => 'SA',
                'languages' => 'es-VE',
            ),
            237 => 
            array (
                'id' => 238,
                'name' => 'Vietnam',
                'currency' => 'VND',
                'code' => 'VN',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Hanoi',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'vi,en,fr,zh,km',
            ),
            238 => 
            array (
                'id' => 239,
            'name' => 'Virgin Islands (British)',
                'currency' => 'USD',
                'code' => 'VG',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Road Town',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-VG',
            ),
            239 => 
            array (
                'id' => 240,
            'name' => 'Virgin Islands (US)',
                'currency' => 'USD',
                'code' => 'VI',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Charlotte Amalie',
                'continentName' => 'North America',
                'continent' => 'NA',
                'languages' => 'en-VI',
            ),
            240 => 
            array (
                'id' => 241,
                'name' => 'Wallis And Futuna Islands',
                'currency' => 'XPF',
                'code' => 'WF',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Mata-Utu',
                'continentName' => 'Oceania',
                'continent' => 'OC',
                'languages' => 'wls,fud,fr-WF',
            ),
            241 => 
            array (
                'id' => 242,
                'name' => 'Western Sahara',
                'currency' => 'MAD',
                'code' => 'EH',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:13:57',
                
                'capital' => 'Laâyoune / El Aaiún',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'ar,mey',
            ),
            242 => 
            array (
                'id' => 243,
                'name' => 'Yemen',
                'currency' => 'YER',
                'code' => 'YE',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Sanaa',
                'continentName' => 'Asia',
                'continent' => 'AS',
                'languages' => 'ar-YE',
            ),
            243 => 
            array (
                'id' => 244,
                'name' => 'Yugoslavia',
                'currency' => '',
                'code' => 'YU',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2016-11-21 05:22:22',
                
                'capital' => '',
                'continentName' => '',
                'continent' => '',
                'languages' => '',
            ),
            244 => 
            array (
                'id' => 245,
                'name' => 'Zambia',
                'currency' => 'ZMW',
                'code' => 'ZM',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Lusaka',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-ZM,bem,loz,lun,lue,ny,toi',
            ),
            245 => 
            array (
                'id' => 246,
                'name' => 'Zimbabwe',
                'currency' => 'ZWL',
                'code' => 'ZW',
                'created_at' => '2016-11-21 05:22:22',
                'updated_at' => '2017-02-27 11:14:09',
                
                'capital' => 'Harare',
                'continentName' => 'Africa',
                'continent' => 'AF',
                'languages' => 'en-ZW,sn,nr,nd',
            ),
        ));
        
        
    }
}