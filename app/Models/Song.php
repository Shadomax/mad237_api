<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'genre_id', 'profile_id', 'description', 'preview', 'file', 'price', 'code', 'picture', 'feature', 'is_remix',  'release_date', 'producer_id', 'views', 'promoted'
    ];

    /**
     * Get the artist that owns the audio.
     */
    public function profile()
    {
        return $this->belongsTo('App\Models\Profile');
    }

    /**
     * Get the producer owning the model.
     */
    public function producer()
    {
        return $this->belongsTo('App\Models\Profile');
    }

     /**
     * Get the genre that owns the audio.
     */
    public function genre()
    {
        return $this->belongsTo('App\Models\Genre');
    }

     /**
     * Get the album that owns the audio.
     */
    public function album()
    {
        return $this->belongsTo('App\Models\Album');
    }
}