<?php

namespace App\Models;

use App\Models\Like;
use App\Models\Album;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements JWTSubject, AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    // protected $appends = ['expires_in'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'date_of_birth', 'gender', 'bio', 'country_id', 'city_id', 'state_id', 'avatar', 'url', 'phone_number' 
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'password'
    ];

    /**
     * The roles that belong to the user.
     */
    public function songs()
    {
        return $this->belongsToMany('App\Models\Song');
    }

    /**
     * Get the playlists for the user.
     */
    public function playlists()
    {
        return $this->hasMany('App\Models\Playlist');
    }

    /**
     * Follow the profile
     *
     * @param integer $user_id
     * @param integer $profile_id
     *
     * @return boolean $result
     */
    public function follow_profile($user_id='', $profile_id='')
    {
        $sql = DB::insert('insert into followings (user_id, profile_id) values (?, ?)', [$user_id, $profile_id]);
        if (!$sql) {
            $result = false;
        }

        $result = $sql;

        return $result;
    }

    /**
     * Like the album
     *
     * @param integer $user_id
     * @param integer $album_id
     *
     * @return boolean $result
     */
    public function like_album($user_id='', $album_id='')
    {
        $album = Album::where('id', $album_id)->first();

        // check if the album exit
        if (!$album) {
            return false;
        }

        // build the like table
        $like = new Like();
        $like->user_id = $user_id;

        // save the like
        $query = $album->likes()->save($like);

        if (!$query) {
            return false;
        }

        return true;
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Review');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Review');
    }
}
