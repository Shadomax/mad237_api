<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',  'sizes', 'colors', 'picture', 'price', 'code', 'description', 'can_ship', 'inventory', 'slug', 'profile_id', 'distribution_center', 'views', 'promoted', 'discount'
    ];

    /**
     * The brand that belong to the product.
     */
    public function profile()
    {
        return $this->belongsTo('App\Models\Profile');
    }

    /**
     * Get all of the product's reviews.
     */
    public function reviews()
    {
        return $this->morphMany('App\Models\Review', 'reviewable');
    }

    /**
     * Get all of the product's photos.
     */
    public function pictures()
    {
        return $this->hasMany('App\Models\Picture');
    }
}
