<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'picture', 'product_id'
    ];

    /**
     * Get the product owning the models.
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
