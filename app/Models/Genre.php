<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'description',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    public function beats()
    {
        return $this->hasMany('App\Models\Beat');
    }

    /**
     * Get the profiles for the genre.
     */
    public function profiles()
    {
        return $this->hasMany('App\Models\Profile');
    }
}
