<?php

namespace App\Transformers;

use App\Models\Playlist;
use Carbon\Carbon;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class PlaylistTransformer extends TransformerAbstract
{
	protected $defaultIncludes = [
        'user'
    ];

	/**
	* Transform a Playlist model into an array
	*
	* @param Playlist $playlist
	* @return array
	*/
	public function transform(Playlist $playlist)
	{
		return [
			'id' => (int) $playlist->id,
			'title' => $playlist->name,
			'slug' => $playlist->slug,
			'description' => $playlist->description,
			'pic' => asset('storage/'. $playlist->picture),
			'created' => Carbon::parse($playlist->created_at)->diffForHumans(),
		];
	}

	public function includeUser(Playlist $playlist)
    {
    	$user = $playlist->user;

        return $this->item($user, new UserTransformer);
    }
}