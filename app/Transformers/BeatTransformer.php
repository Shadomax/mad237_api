<?php

namespace App\Transformers;

use App\Models\Beat;
use Carbon\Carbon;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class BeatTransformer extends TransformerAbstract
{
	protected $defaultIncludes = [
        'profile', 'genre'
    ];

	/**
	* Transform a Beat model into an array
	*
	* @param Beat $beat
	* @return array
	*/
	public function transform(Beat $beat)
	{
		return [
			'id' => (int) $beat->id,
			'name' => $beat->title,
			'unq_code' => $beat->code,
			'price' => $beat->price,
			'bit_per_minute' => $beat->bpm,
			'description' => $beat->description,
			'promote' => $beat->is_promoted,
			'readmore' => Str::words($beat->description, $words = 50, $end = '...'),
			'created' => Carbon::parse($beat->created_at)->toIso8601String(),
			'pic' => asset('storage/'. $beat->picture),
			'url' => asset('storage/'. $beat->preview),
			'file' => asset('storage/'. $beat->file),
			'tags' => explode(',', $beat->tags),
		];
	}

	public function includeProfile(Beat $beat)
    {
    	$profile = $beat->profile;

        return $this->item($profile, new ProfileTransformer);
    }

    public function includeGenre(Beat $beat)
    {
    	$genre = $beat->genre;

        return $this->item($genre, new GenreTransformer);
    }
}