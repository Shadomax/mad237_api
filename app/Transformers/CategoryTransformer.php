<?php

namespace App\Transformers;

use App\Models\Category;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
	/**
     * List of resources to automatically include
     *
     * @var array
     */

	// $defaultIncludes

    protected $availableIncludes = [
        'blogs'
    ];

	/**
	* Transform a Category model into an array
	*
	* @param Category $category
	* @return array
	*/
	public function transform(Category $category)
	{
		return [
			'id' => (int) $category->id,
			'title' => $category->name,
			'slug' => $category->slug,
			'description' => $category->description,
			'total_blog' => $category->blogs->count(),
			'created' => Carbon::parse($category->created_at)->toIso8601String(),
			// 'updated' => Carbon::parse($category->updated_at)->toIso8601String(),
		];
	}

	/**
     * Include Blogs
     *
     * @param Category $category
     * @return \League\Fractal\Resource\Collection
     */
    public function includeBlogs(Category $category)
    {
        $blogs = $category->blogs;

        return $this->collection($blogs, new BlogTransformer);
    }
}