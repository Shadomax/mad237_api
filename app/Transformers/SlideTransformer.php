<?php

namespace App\Transformers;

use App\Models\Slide;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class SlideTransformer extends TransformerAbstract
{
	/**
	* Transform a Slide model into an array
	*
	* @param Slide $slide
	* @return array
	*/
	public function transform(Slide $slide)
	{
		return [
			'id' => (int) $slide->id,
			'title' => $slide->title,
			'link' => $slide->link,
			'link_name' => $slide->link_name,
			'created' => Carbon::parse($slide->created_at)->toIso8601String(),
			// 'updated' => Carbon::parse($slide->updated_at)->toIso8601String(),
			'pic' => asset('storage/'. $slide->picture),
		];
	}
}