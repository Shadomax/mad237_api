<?php

namespace App\Providers;

use App\User;
use App\Venture;
use App\Investor;
use App\Administrator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            $client = $_SERVER['HTTP_CLIENT_TYPE'];

            $apiToken = $request->header('API_TOKEN');

            if ($request->header('API_TOKEN') !== null) {
                if ($client == 'investor') {
                    return Investor::where('api_token', $request->input('api_token'))->first();
                } elseif ($client == 'venture') {
                    return Venture::where('api_token', $request->input('api_token'))->first();
                } elseif ($client == 'administrator') {
                    return Administrator::where('api_token', $request->input('api_token'))->first();
                } elseif ($client == 'individual') {
                    return User::where('api_token', $apiToken)->first();
                } else {
                    return response('Unauthorized: no client type does not exist', 401);
                }
            }
        });
    }
}
