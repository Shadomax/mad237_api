<?php

namespace App\Http\Middleware;

use Closure;

class ExampleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset($_SERVER['HTTP_CLIENT_TYPE'])){  
            return response()->json(['message' => 'Web browse are not allowed to view this routes'], 401);  
        }

        return $next($request);
    }
}
