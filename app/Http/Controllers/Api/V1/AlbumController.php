<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Song;
use App\Models\Album;
use App\Models\Video;
use App\Models\Genre;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\SongTransformer;
use App\Transformers\VideoTransformer;
use App\Transformers\AlbumTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AlbumController extends Controller
{

    function __construct()
    {

    }

    public function get_albums(Request $request)
    {
        if ($request->exists('profile_id')) {
            $paginator = Album::where('profile_id', $request->profile_id)->orderBy('created_at', 'desc')->paginate(($request->has('pagination')) ? $request->pagination : 20);
        } else {
            $paginator = Album::orderBy('created_at', 'desc')->paginate(($request->has('pagination')) ? $request->pagination : 20);
        }

        // Get transformed array of data
        return $this->response->paginator($paginator, new AlbumTransformer);
    }

    public function get_album_by_slug($slug)
    {
        $data = Album::where('slug', $slug)->first();

        if (!$data) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Album Not Found');
        }

        $resource = new Item($data, $this->albumTransformer);

        if ($data->type == 'song') {
            $this->fractal->parseIncludes(['songs', 'producer', 'profile']);
        } elseif ($data->type == 'video') {
            $this->fractal->parseIncludes(['videos', 'producer', 'profile']);
        }

        $resource = $this->fractal->createData($resource);

        return $this->data($resource->toArray(), 200);
    }

    public function get_songs(Request $request)
    {
        if ($request->exists('genre_id')) {
            if (!empty($request->genre_id)) {
                $paginator = Song::where('genre_id', $request->genre_id)->paginate(($request->has('pagination')) ? $request->pagination : 20);
            }
        } else {
            $paginator = Song::paginate(($request->has('pagination')) ? $request->pagination : 20);
        }

        // Get transformed array of data
        return $this->response->paginator($paginator, new SongTransformer);
    }

    public function get_song_by_id($id)
    {
        $data = Song::where('id', $id)->first();

        if (!$data) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Song Not Found');
        }

        return $this->response->item($data, new SongTransformer);
    }

    public function get_latest_songs(Request $request)
    {
        $data = Song::orderBy('created_at', 'desc')->limit(2)->get();

        // Create a resource collection transformer
        $resource = new Collection($data, $this->songTransformer);

        // parse includes
        // $this->fractal->parseIncludes(['profile', 'album', 'producer', 'genre']);

        // Transform data
        $resource = $this->fractal->createData($resource);

        // Get transformed array of data
        return $this->data($resource->toArray(), 200);
    }
}
