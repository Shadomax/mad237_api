<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Blog;
use App\Models\Song;
use App\Models\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\BlogTransformer;
use App\Transformers\SongTransformer;
use App\Transformers\ProfileTransformer;

class LatestController extends Controller
{
    public function get_blogs(Request $request)
    {
       
        $data = Blog::orderBy('created_at', 'desc')->limit(2)->get();

        return $this->response->collection($data, new BlogTransformer);
    }

    public function get_profiles(Request $request)
    {
       
        $data = Profile::orderBy('created_at', 'desc')->limit(2)->get();

        return $this->response->collection($data, new ProfileTransformer);
    }

    public function get_songs(Request $request)
    {
       
        $data = Song::orderBy('created_at', 'desc')->limit(2)->get();

        return $this->response->collection($data, new SongTransformer);
    }
}
