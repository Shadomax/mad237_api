<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Blog;
use App\Models\Comment;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Transformers\BlogTransformer;
use App\Http\Controllers\Controller;
use App\Transformers\CategoryTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
// use Illuminate\Support\Facades\Hash;

class BlogController extends Controller
{

    function __construct()
    {
        
    }

    public function get_blogs(Request $request)
    {
        if ($request->exists('category_id')) {
            if (!empty($request->category_id)) {
                $paginator = Blog::where('category_id', $request->category_id)->orderBy('created_at', 'desc')->paginate(($request->has('pagination')) ? $request->pagination : 20);
            }
        } else {
            $paginator = Blog::orderBy('created_at', 'desc')->paginate(($request->has('pagination')) ? $request->pagination : 20);
        }
        
        return $this->response->paginator($paginator, new BlogTransformer);
    }

    public function get_blog_by_slug($slug)
    {
        $data = Blog::where('slug', $slug)->first();

        if (!$data) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Blog Not Found');
        }

        return $this->response->item($data, (new BlogTransformer)->setDefaultIncludes(['category', 'related', 'comments']));
    }

    public function get_categories()
    {
        $data = Category::all();

        return $this->response->collection($data, new CategoryTransformer);
    }

    public function get_category_by_slug($slug)
    {
        $data = Category::where('slug', $slug)->first();

        if (!$data) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Category Not Found');
        }

        return $this->response->item($data, (new CategoryTransformer)->setDefaultIncludes(['blogs']));
    }

    public function add_blog_comment(Request $request)
    {
        $this->validate($request, [
            'blog_id' => 'bail|required|integer',
            'name' => 'bail|required|min:6',
            'email' => 'bail|required|email',
            'comment' => 'bail|required|min:20',
        ]);

        $data = Comment::create([
            'blog_id' => $request->blog_id,
            'name' => $request->name,
            'email' => $request->email,
            'content' => $request->comment
        ]);

        if (!$data) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to submit comment at the moment');
        }

        return $this->success('Comment submitted');
        
    }
}
