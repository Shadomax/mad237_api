<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Slide;
use App\Http\Controllers\Controller;
use App\Transformers\SlideTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SlideController extends Controller
{

    function __construct()
    {
        
    }

    public function get_slides()
    {
        $slides = Slide::all(); // Get users from DB
        return $this->response->collection($slides, new slideTransformer);
    }
}
