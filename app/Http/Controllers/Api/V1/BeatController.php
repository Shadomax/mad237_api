<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Beat;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\BeatTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BeatController extends Controller
{

    function __construct()
    {
        
    }

    public function get_beats(Request $request)
    {
        if ($request->exists('genre_id')) {
            if (!empty($request->genre_id)) {
                $paginator = Beat::where('genre_id', $request->genre_id)->orderBy('created_at', 'desc')->paginate(($request->has('pagination')) ? $request->pagination : 20);
            }
        } else {
            $paginator = Beat::orderBy('created_at', 'desc')->paginate(($request->has('pagination')) ? $request->pagination : 20);
        }

        // Get transformed array of data
        return $this->response->paginator($paginator, new BeatTransformer);
    }

    public function get_product_by_slug($slug)
    {
        $data = Product::where('slug', $slug)->first();

        if (!$data) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Beat Not Found');
        }

        return $this->response->item($data, (new BeatTransformer)->setDefaultIncludes(['pictures', 'reviews']));
    }

    public function add_beat_review(Request $request)
    {
        $this->validate($request, [
            'review_id' => 'bail|required|integer',
            'name' => 'bail|required|min:6',
            'email' => 'bail|required|email',
            'review' => 'bail|required|min:20',
        ]);

        $product = Review::findOrFail($request->product_id);

        if (!$product) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Beat Not Found');
        }

        $review = new Review();

        // bind the review data
        $review->name = $request->name;
        $review->email = $request->email;
        $review->content = $request->review;

        if (!$product->reviews()->save($review)) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to save review');
        }

        return $this->success('Added review');
    }
}
