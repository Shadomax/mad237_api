<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Review;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\ProductTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductController extends Controller
{

    function __construct()
    {
        
    }

    public function get_products(Request $request)
    {
        if ($request->exists('profile_id')) {
            if (!empty($request->profile_id)) {
                $paginator = Product::where('profile_id', $request->profile_id)->orderBy('created_at', 'desc')->paginate(($request->has('pagination')) ? $request->pagination : 20);
            }
        } else {
            $paginator = Product::orderBy('created_at', 'desc')->paginate(($request->has('pagination')) ? $request->pagination : 20);
        }
        
        return $this->response->paginator($paginator, new ProductTransformer);
    }

    public function get_product_by_slug($slug)
    {
        $data = Product::where('slug', $slug)->first();

        if (!$data) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Products Not Found');
        }

        return $this->response->item($data, (new ProductTransformer)->setDefaultIncludes(['profile', 'pictures', 'related', 'reviews']));
    }

    public function add_product_review(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'bail|required|integer',
            'name' => 'bail|required|min:6',
            'email' => 'bail|required|email',
            'review' => 'bail|required|min:20',
        ]);

        $product = Product::findOrFail($request->product_id);

        if (!$product) {
            return $this->error('resource not found', 404);
        }

        $review = new Review();

        // bind the review data
        $review->name = $request->name;
        $review->email = $request->email;
        $review->content = $request->review;

        if (!$product->reviews()->save($review)) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Unable to submit review at the moment');
        }

        return $this->success('Review submitted');
    }
}
