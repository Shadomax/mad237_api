<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    public function save_beat(Request $request)
    {
    	$this->validate($request, [
    		'beat_id' => 'required|integer',
    		'name' => 'required|string',
    		'email' => 'required|string|email'
    	]);

    	
    }
}
